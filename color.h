#define colorjh
//#define colordracula
#ifdef colorjh
/* Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 15;
unsigned int defaultbg = 0;
static unsigned int defaultcs = 15;
static unsigned int defaultrcs = 0;
static const char *colorname[] = {
	"#0e1010", // hard contrast: #1d2021 / soft contrast: #32302f
	"#cc241d",
	"#98971a",
	"#d79921",
	"#458588",
	"#b16286",
	"#689d6a",
	"#a89984",
	"#928374",
	"#fb4934",
	"#b8bb26", //
	"#1c4f1f", // dark green vim search highlight
	"#83a598",
	"#d3869b",
	"#8ec07c",
	"#ebdbb2",
	[255] = 0,
	// more colors can be added after 255 to use with DefaultXX
	"black",   // 256 -> bg
	"white",   // 257 -> fg
};
#endif
#ifdef colordracula
/* Default colors (colorname index)
 * foreground, background, cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;
static unsigned int defaultrcs = 257;
/*
 * Colors used, when the specific fg == defaultfg. So in reverse mode this
 * will reverse too. Another logic would only make the simple feature too
 * complex.
 */
unsigned int defaultitalic = 7;
unsigned int defaultunderline = 7;
static const char *colorname[] = {
	// normal
	"#000000", // black
	"#ff5555", // red
	"#50fa7b", // green
	"#f1fa8c", // yellow
	"#bd93f9", // blue
	"#ff79c6", // magenta
	"#8be9fd", // cyan
	"#bbbbbb", // white
	// bright
	"#44475a",
	"#ff5555",
	"#50fa7b",
	"#f1fa8c",
	"#bd93f9",
	"#ff79c6",
	"#8be9fd",
	"#ffffff",
	// special
	[256] = "#282a36", // bg
	[257] = "#f8f8f2", // fg
};
#endif
