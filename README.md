# st - the simple (suckless) terminal from [here](https://github.com/LukeSmithxyz/st) and [here](https://st.suckless.org/)

+ Compatibility with `Xresources` and `pywal` for dynamic colors. The `Xdefaults` file shows a usage example.
+ Default [gruvbox](https://github.com/morhetz/gruvbox) colors otherwise.
+ Transparency/alpha, which is also adjustable from `~/.Xresources`.
+ Default font is system "mono" at 16pt, meaning the font will match your system font.
+ Very useful keybinds including:
	+ Copy is alt-c, paste is alt-v or alt-p  Shift-Insert pastes from primary selection
	+ Alt-l feeds all urls on screen to dmenu, so they user can choose and
	  follow one (requires dmenu installed).
	+ Zoom in/out or increase font size with <Ctrl>+/- or <Alt><Shift>u/d for larger intervals.
	+ Scrollong with mouse wheel or <Alt>↑/↓/k/j
	+ <Shift>/<Alt><PgUp>/<PgDn> || <Alt>u/d to scroll one page
	+ Return to default zoom: <Alt><Home>
+ Vertcenter
+ Scrollback
+ updated to latest version 0.8.1

## Dependencies
- `make`
`-dev` versions of
- at least `libxft`
probably
- `fontconfig`
- `libX11`

## Installation
- install libxft-dev
```
make
sudo make install
```

### Additional Infos
On OpenBSD, be sure to edit `config.mk` first and remove `-lrt` from the `$LIBS` before compiling.

Be sure to have a composite manager (`xcompmgr`, `compton`, etc.) running if you want transparency.

## How to configure dynamically with Xresources

For many key variables, this build of `st` will look for X settings set in either `~/.Xdefaults` or `~/.Xresources`. You must run `xrdb` on one of these files to load the settings.

For example, you can define your desired fonts, transparency or colors:

```
*.font:	Liberation Mono:pixelsize=12:antialias=true:autohint=true;
*.alpha: 150
*.color0: #111
...
```

The `alpha` value (for transparency) goes from `0` (transparent) to `255`
(opaque).

### Colors

To be clear about the color settings:

- This build will use gruvbox colors by default and as a fallback.
- If there are Xresources colors defined, those will take priority.
- But if `wal` has run in your session, its colors will take priority.

Note that when you run `wal`, it will negate the transparency of existing windows, but new windows will continue with the previously defined transparency.

